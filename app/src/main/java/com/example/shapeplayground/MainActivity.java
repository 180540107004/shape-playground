package com.example.shapeplayground;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Button undo,stats;
    ImageButton circle,square,triangle;
    ImageView shape,shapesquare,shapetriangle;
    FrameLayout frameshape;
    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        undo = findViewById(R.id.btn_undo);
        stats = findViewById(R.id.btn_stats);
        circle = findViewById(R.id.btn_circle);
        square = findViewById(R.id.btn_square);
        triangle = findViewById(R.id.btn_triangle);
        shape = findViewById(R.id.shape);
        shapesquare = findViewById(R.id.shapesquare);
        shapetriangle = findViewById(R.id.shapetriangle);
        frameshape = findViewById(R.id.frameShape);
        ll = findViewById(R.id.ll);
      //  int[] locations = new int[2];
        final DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) frameshape.getLayoutParams();
        ViewGroup.LayoutParams layoutParamsq = shapesquare.getLayoutParams();
        ViewGroup.LayoutParams layoutParamst = shapetriangle.getLayoutParams();
        int width = displaymetrics.widthPixels;
        int height = displaymetrics.heightPixels;
        circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(shape.getVisibility() == View.GONE) {
                    shape.setVisibility(View.VISIBLE);
                }
                Random random = new Random();
                int imageX = random.nextInt(width);
                int imageY = random.nextInt(height);
                shape.setX(imageX);
                shape.setY(imageY);
                frameshape.addView(shape,layoutParams);
         }
        });

        square.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(shapesquare.getVisibility() == View.GONE) {
                    shapesquare.setVisibility(View.VISIBLE);
                }
                Random random = new Random();
                int imageX = random.nextInt(width);
                int imageY = random.nextInt(height);
                shapesquare.setX(imageX);
                shapesquare.setY(imageY);

            }
        });

        triangle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(shapetriangle.getVisibility() == View.GONE) {
                    shapetriangle.setVisibility(View.VISIBLE);
                }
                Random r = new Random();
                layoutParamst.width =  r.nextInt(width );
                layoutParamst.height =  r.nextInt(height );
                shapetriangle.setLayoutParams(layoutParamst);

            }
        });

        stats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, Stats_Activity.class);
                in.putExtra("From", "New");
                startActivity(in);
            }
        });
    }
}